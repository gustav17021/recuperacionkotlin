package com.armenta.recuperacionkotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

//Generación de aplicación.


class MainActivity : AppCompatActivity() {
    private var txtNombreCliente: EditText? = null
    private var btnCotización: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnCotización!!.setOnClickListener { entrar() }
    }

    private fun iniciarComponentes() {
        txtNombreCliente = findViewById<View>(R.id.txtNombreCliente) as EditText
        btnCotización = findViewById<View>(R.id.btnCotización) as Button
    }

    private fun entrar() {
        val nombre = txtNombreCliente!!.text.toString().trim { it <= ' ' }
        if (nombre.isEmpty()) {
            Toast.makeText(
                this.applicationContext,
                "Ingrese el nombre del cliente",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val bundle = Bundle()
            bundle.putString("nombre", txtNombreCliente!!.text.toString())
            val intent = Intent(this@MainActivity, CotizacionActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}//Se reviso y se realizaron ajustes

