package com.armenta.recuperacionkotlin
//Se realizó la clase de cotizacion.
class Cotizacion(
    folio: Int,
    descripcion: String,
    valorAuto: Float,
    porEngacnhe: Float,
    plazo: Int
) {
    private var folio: Int
    private var descripcion: String
    private var valorAuto: Float
    private var porEnganche: Float
    private var plazo: Int

    init {
        this.folio = folio
        this.descripcion = descripcion
        this.valorAuto = valorAuto
        porEnganche = porEngacnhe
        this.plazo = plazo
    }

    //getter
    fun getFolio(): Int {
        return folio
    }

    fun getDescripcion(): String {
        return descripcion
    }

    fun getValorAuto(): Float {
        return valorAuto
    }

    fun getPorEngacnhe(): Float {
        return porEnganche
    }

    fun getPlazo(): Int {
        return plazo
    }

    //setter
    fun setFolio(folio: Int) {
        this.folio = folio
    }

    fun setDescripcion(descripcion: String) {
        this.descripcion = descripcion
    }

    fun setValorAuto(valorAuto: Float) {
        this.valorAuto = valorAuto
    }

    fun setPorEnganche(porEngacnhe: Float) {
        porEnganche = porEnganche
    }

    fun setPlazo(plazo: Int) {
        this.plazo = plazo
    }

    fun generarFolio(): Int {
        folio += 1
        return folio
    }

    fun calcularEnganche(): Float {
        val enganche: Float
        enganche = (valorAuto * .25).toFloat()
        return enganche
    }

    fun calcularPagoMensual(): Float {
        var pagoMensual = 0.0f
        val plazo = 0
        if (plazo == 1) {
            pagoMensual = valorAuto - calcularEnganche() / 12
        } else if (plazo == 2) {
            pagoMensual = valorAuto - calcularEnganche() / 18
        } else if (plazo == 3) {
            pagoMensual = valorAuto - calcularEnganche() / 24
        } else if (plazo == 4) {
            pagoMensual = valorAuto - calcularEnganche() / 36
        }
        return pagoMensual
    }
}//Se reviso y se realizaron ajustes


