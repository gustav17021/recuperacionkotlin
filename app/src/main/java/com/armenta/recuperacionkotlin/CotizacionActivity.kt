package com.armenta.recuperacionkotlin

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

//Se agregó CotizacionActivity


class CotizacionActivity : AppCompatActivity() {
    private var txtDescripcion: EditText? = null
    private var txtValorAuto: EditText? = null
    private var txtPorcentaje: EditText? = null
    private var rdbMeses: RadioGroup? = null
    private var txtFolio: TextView? = null
    private var txtNombre: TextView? = null
    private var lblPagoMensual: TextView? = null
    private var lblEnganche: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cotizacion)
        iniciar()
        val datos = intent.extras
        val usuario = datos!!.getString(MainActivity.EXTRA_MESSAGE)
        generarFol()
        txtNombre!!.text = usuario
    }

    private fun iniciar() {
        txtFolio = findViewById(R.id.lblFolio)
        txtNombre = findViewById(R.id.lblCliente)
        txtDescripcion = findViewById<EditText>(R.id.txtDescripcion)
        txtValorAuto = findViewById<EditText>(R.id.txtValorAuto)
        txtPorcentaje = findViewById<EditText>(R.id.txtPorEng)
        rdbMeses = findViewById<RadioGroup>(R.id.rdbMeses)
        lblPagoMensual = findViewById<TextView>(R.id.lblPagoM)
        lblEnganche = findViewById<TextView>(R.id.lblEnganche)
    }

    fun generarFol() {
        val numeroAleatorio = Math.random()

        // Multiplica el número aleatorio por un rango para obtener un número en un rango específico
        val minimo = 1
        val maximo = 1000
        val rango = maximo - minimo + 1
        val folio = (numeroAleatorio * rango).toInt() + minimo
        txtFolio!!.text = folio.toString()
    }

    fun calcular(v: View?) {
        // Obtener los valores de entrada de los EditTexts
        val selectedRadioButton = findViewById<RadioButton>(rdbMeses!!.checkedRadioButtonId)
        if (txtDescripcion!!.text.toString() == "" || txtValorAuto!!.text.toString() == "" || selectedRadioButton == null || txtPorcentaje!!.text.toString() == "") {
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show()
        } else {
            val folio = txtFolio!!.text.toString().toInt()
            val descripcion = txtDescripcion!!.text.toString()
            val valorAuto = txtValorAuto!!.text.toString().toFloat()
            val porEnganche = txtPorcentaje!!.text.toString().toFloat()
            var plazo = 0
            val puestoText = selectedRadioButton.text.toString()
            if (porEnganche < 1 || porEnganche > 100) {
                Toast.makeText(this, "Porcentaje erroneo", Toast.LENGTH_SHORT).show()
            } else {
                if (puestoText.equals("12 meses", ignoreCase = true)) {
                    plazo = 12
                } else if (puestoText.equals("18 meses", ignoreCase = true)) {
                    plazo = 18
                } else if (puestoText.equals("24 meses", ignoreCase = true)) {
                    plazo = 24
                } else if (puestoText.equals("36 meses", ignoreCase = true)) {
                    plazo = 36
                }
                val Coti = Cotizacion(folio, descripcion, valorAuto, porEnganche, plazo)
                val enganche = Coti.calcularEnganche()
                val pagom: Float = Coti.calcularPago()
                lblEnganche!!.text = enganche.toString()
                lblPagoMensual!!.text = pagom.toString()
            }
        }
    }

    fun limpiar(v: View?) {
        txtNombre = findViewById(R.id.lblCliente)
        txtDescripcion!!.setText("")
        txtValorAuto!!.setText("")
        txtPorcentaje!!.setText("")
        lblPagoMensual!!.text = ""
        lblEnganche!!.text = ""
    }

    fun regresar(v: View?) {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar a la vista pricipal?")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which -> dialog.dismiss() }
        confirmar.show()
    }
}//Se reviso y se realizaron ajustes
